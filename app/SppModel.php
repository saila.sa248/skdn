<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SppModel extends Model
{
    protected $table = 'spp';

    protected $fillable = ['id_spp','nim','nominal','bulan_pembayaran','tgl_pembayaran','penerima','status','keterangan'];

    public function HaveMahasiswa()
    {
        return $this->belongsTo(MahasiswaModel::class, 'nim', 'nim');
    }
}