<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MahasiswaModel;
use App\SppModel;
use App\SaldoModel;
use App\User;

class SPPController extends Controller
{
    public function index(){
        $data = MahasiswaModel::all()->where('status',1);
        $dataSPP = SppModel::all();
        $total_spp = SppModel::all()->sum('nominal');
        $total_mhs = SppModel::where('status',1)->count('id_spp');
        $idspp = SPPModel::latest('id_spp')->get()->first();
        $penerima = User::all();
        
        return view('admin.pemasukan.spp.index',compact('total_spp','dataSPP','data','total_mhs','idspp','penerima','xspp'));
    }

    public function store(Request $request, SppModel $sppmodel){
        
        $simpan = $sppmodel->create([
            'id_spp' => $request->id_spp,
            'nim' => $request->nim,
            'bulan_pembayaran' => $request->bulan_pembayaran,
            'tgl_pembayaran' => $request->tgl_pembayaran,
            'nominal' => $request->nominal,
            'penerima' => $request->penerima,
            'keterangan' => $request->keterangan,
            'status' => 1
        ]);

        $x_spp = $request->x_spp;
        $total_xspp = $x_spp - 1;
        
        $datamahasiswa = MahasiswaModel::where('nim', $request->nim)->update([
            'id_spp' => $request->id_spp,
            'x_spp' => $total_xspp,
        ]);

        $saldo_akhir = SaldoModel::latest('nomor')->first();
        $saldo_akhir = $saldo_akhir->saldo;
        $total = $saldo_akhir + $request->nominal;
        
        // {{route('/sendemail')}}

        $saldo = SaldoModel::create([
            'jumlah' => $request->nominal,
            'keterangan' => 'SPP',
            'kategori' => 'Pemasukan',
            'saldo' => $total,
            'created_at' => now(),
        ]);

        if (!$simpan->exists) {
            return redirect()->route('spp')->with('error', 'Data Gagal Disimpan');
        }
        return redirect()->route('spp')->with('success', 'Data Berhasil Disimpan');
    }
}