<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MahasiswaModel;
use App\WisudaModel;
use App\SaldoModel;
use App\User;

class WisudaController extends Controller
{
    public function index(){
        $datamahasiswa = MahasiswaModel::all()->where('bayarwisuda',2);
        $data = WisudaModel::all();
        $total_pembayaranwisuda = WisudaModel::all()->sum('nominal');
        $total_mhs = MahasiswaModel::where('status',1)->count('id_pembayaranwisuda');
        $id_pembayaranwisuda = WisudaModel::latest('id_pembayaranwisuda')->get()->first();
        $penerima = User::all();
        
        return view('admin.pemasukan.wisuda.index', compact('datamahasiswa','data','total_pembayaranwisuda','total_mhs','id_pembayaranwisuda','penerima'));
    }

    public function store(Request $request, WisudaModel $wisudamodel){
        $this->validate($request,[
            'nim' => 'required',
            'tgl_pembayaran' => 'required',
            'nominal' => 'required|numeric',
        ],[
            'nim.required' => 'Mahasiswa Harus Diisi',
            'tgl_pembayaran.required' => 'Tanggal Pembayran Harus Diisi',
            'nominal.required' => 'Nominal Harus Diisi',
            'nominal.numeric' => 'Nominal Harus Berupa Angka'
        ]);
        $simpan = $wisudamodel->create([
            'id_pembayaranwisuda' => $request->id_pembayaranwisuda,
            'nim' => $request->nim,
            'tgl_pembayaran' => $request->tgl_pembayaran,
            'nominal' => $request->nominal,
            'keterangan' => $request->keterangan,
            'penerima' => $request->penerima,
            'status' => 1
        ]);

        $saldo_akhir = SaldoModel::latest('nomor')->first();
        $saldo_akhir = $saldo_akhir->saldo;
        $total = $saldo_akhir + $request->nominal;
        
        $saldo = SaldoModel::create([
            'jumlah' => $request->nominal,
            'keterangan' => 'Wisuda',
            'kategori' => 'Pemasukan',
            'saldo' => $total,
            'created_at' => now(),
        ]);

        $datacalonmhs = MahasiswaModel::where('nim', $request->nim)->update([
            'id_pembayaranwisuda' => $request->id_pembayaranwisuda,
        ]);

        if (!$simpan->exists) {
            toastr()->error('Pembayaran Wisuda Gagal');
            return redirect()->route('wisuda')->with('error', 'Data Gagal Disimpan');
        }
        toastr()->success('Pembayaran Wisuda Berhasil');
        return redirect()->route('wisuda')->with('success', 'Data Berhasil Disimpan');
    }
}