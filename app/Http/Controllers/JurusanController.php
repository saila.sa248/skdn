<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JurusanModel;

class JurusanController extends Controller
{
    public function index(Request $request){
        $data = JurusanModel::all()->where('status',1);
        $idj = JurusanModel::latest()->get()->first();

        return view('mahasiswa.jurusan', compact('data','idj'));
    }

    public function postjurusan(Request $request, JurusanModel $jurusanmodel){
        $simpan = $jurusanmodel->create([
            'id_jurusan' => $request->id_jurusan,
            'nm_jurusan' => $request->nm_jurusan,
            'status' => 1
        ]);

        if (!$simpan->exists) {
            return redirect()->route('jurusan')->with('error', 'Data Gagal Disimpan');
        }
        return redirect()->route('jurusan')->with('success', 'Data Berhasil Disimpan');
    }
}