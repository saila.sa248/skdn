<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DaftarModel;
use App\CalonmhsModel;
use App\MahasiswaModel;
use App\SaldoModel;
use App\User;
use DB;

class DaftarController extends Controller
{
    public function index(){
        $datacalon = CalonmhsModel::all()->where('bayardaftar',2);
        $data = DaftarModel::all()->where('status',1);
        $total_pendaftaran = DaftarModel::all()->sum('nominal');
        $total_mhs = DaftarModel::where('status',1)->count('id_calonmhs');
        $idpembayaran = DaftarModel::latest('id_pembayaran')->get()->first();
        $penerima = User::all();
        
        return view('admin.pemasukan.pendaftaran.index', compact('data','datacalon','total_pendaftaran','total_mhs','idpembayaran','penerima'));
    }

    public function store(Request $request, DaftarModel $daftarmodel){
        $this->validate($request,[
            'id_calonmhs' => 'required',
            'tgl_pembayaran' => 'required',
            'nominal' => 'required|numeric',
        ],[
            'id_calonmhs.required' => 'ID Calon Mahasiswa Harus Diisi',
            'tgl_pembayaran.required' => 'Tanggal Pembayran Harus Diisi',
            'nominal.required' => 'Nominal Harus Diisi',
            'nominal.numeric' => 'Nominal Harus Berupa Angka'
        ]);
        $simpan = $daftarmodel->create([
            'id_pembayaran' => $request->id_pembayaran,
            'id_calonmhs' => $request->id_calonmhs,
            'tgl_pembayaran' => $request->tgl_pembayaran,
            'nominal' => $request->nominal,
            'keterangan' => $request->keterangan,
            'penerima' => $request->penerima,
            'status' => 1
        ]);
        
        // INPUT KE TABLE SALDO
        // 1. NOMINAL
        // 2. KETERANGAN (PENDAFTARAN)
        // 3. INSERT KE TABEL SALDO
        $saldo_akhir = SaldoModel::latest('nomor')->first();
        $saldo_akhir = $saldo_akhir->saldo;
        $total = $saldo_akhir + $request->nominal;
        
        $saldo = SaldoModel::create([
            'jumlah' => $request->nominal,
            'keterangan' => 'Pendaftaran',
            'kategori' => 'Pemasukan',
            'saldo' => $total,
            'created_at' => now(),
        ]);

        $datacalonmhs = CalonmhsModel::where('id_calonmhs', $request->id_calonmhs)->update([
            'bayardaftar' => 1,
        ]);

        if (!$simpan->exists) {
            toastr()->error('Tambah Data Pendaftaran Gagal');
            return redirect()->route('daftar')->with('error', 'Data Gagal Disimpan');
        }
        toastr()->success('Pendaftaran Berhasil');
        return redirect()->route('daftar')->with('success', 'Data Berhasil Disimpan');
    }

    public function invoice($id){
        $inv = DaftarModell::where('id_pembayaran',$id)->get();
        return view('admin.pemasukan.pendaftaran.index', compact('inv'));
    }
}