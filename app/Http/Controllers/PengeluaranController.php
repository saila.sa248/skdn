<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MahasiswaModel;
use App\DaftarModel;
use App\SeminarModel;
use App\SidangModel;
use App\WisudaModel;
use App\SPPModel;
use App\PengeluaranModel;
use App\SaldoModel;
use App\User;

class PengeluaranController extends Controller
{
    public function index(){
        $total_Pendaftaran = DaftarModel::all()->sum('nominal');
        $total_seminar = SeminarModel::all()->sum('nominal');
        $total_sidang = SidangModel::all()->sum('nominal');
        $total_wisuda = WisudaModel::all()->sum('nominal');
        $total_spp = SPPModel::all()->sum('nominal');
        $idpengeluaran = PengeluaranModel::latest('kode_pengeluaran')->first();
        $total_saldo = SaldoModel::latest('nomor')->first();
        $total_saldo = $total_saldo->saldo;
        $pengeluaran = PengeluaranModel::all();
        $tot_pengeluaran = PengeluaranModel::all()->sum('nominal');
        $penerima = User::all();

        return view('admin.pengeluaran.index',compact('penerima','total_saldo','pengeluaran','tot_pengeluaran','idpengeluaran'));
    }

    public function store(Request $request, PengeluaranModel $pengeluaranmodel){
        $this->validate($request,[
            'kategori' => 'required',
            'tgl_pengeluaran' => 'required',
            'nominal' => 'required|numeric',
            'penerima' => 'required',
            'keterangan' => 'required'
        ],[
            'kategori.required' => 'Kategori Harus Diisi',
            'tgl_pengeluaran.required' => 'Tanggal Pengeluaran Harus Diisi',
            'nominal.required' => 'Nominal Harus Diisi',
            'nominal.numeric' => 'Nominal Harus Berupa Angka',
            'penerima.required' => 'Penerima Harus Diisi',
            'keterangan.required' => 'Keterangan Wajib Diisi'
        ]);

        $simpan = $pengeluaranmodel->create([
            'kode_pengeluaran' => $request->kode_pengeluaran,
            'kategori' => $request->kategori,
            'tgl_pengeluaran' => $request->tgl_pengeluaran,
            'nominal' => $request->nominal,
            'keterangan' => $request->keterangan,
            'penerima' => $request->penerima,
            'p_jawab' => $request->p_jawab,
            'status' => 1
        ]);

        $saldo_akhir = SaldoModel::latest('nomor')->first();
        $saldo_akhir = $saldo_akhir->saldo;
        $total = $saldo_akhir - $request->nominal;
        
        $saldo = SaldoModel::create([
            'jumlah' => $request->nominal,
            'kategori' => 'Pengeluaran',
            'keterangan' => $request->kategori,
            'saldo' => $total,
            'created_at' => now(),
        ]);

        if (!$simpan->exists) {
            toastr()->error('Tambah Data Pengeluaran Gagal');
            return redirect()->route('pengeluaran')->with('error', 'Data Gagal Disimpan');
        }
        toastr()->success('Tambah Data Pengeluaran Berhasil');
        return redirect()->route('pengeluaran')->with('success', 'Data Berhasil Disimpan');
    }
}