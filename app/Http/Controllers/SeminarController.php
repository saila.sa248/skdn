<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MahasiswaModel;
use App\SeminarModel;
use App\SaldoModel;
use App\User;

class SeminarController extends Controller
{
    public function index(){
        $datamahasiswa = MahasiswaModel::all()->where('bayarseminarKP',2);
        $data = SeminarModel::all();
        $total_pembayaranseminar = SeminarModel::all()->sum('nominal');
        $total_mhs = MahasiswaModel::where('status',1)->count('id_pembayaranseminar');
        $id_pembayaranseminar = SeminarModel::latest('id_pembayaranseminar')->get()->first();
        $penerima = User::all();
        
        return view('admin.pemasukan.seminar.index', compact('datamahasiswa','data','total_pembayaranseminar','total_mhs','id_pembayaranseminar','penerima'));
    }

    public function store(Request $request, SeminarModel $seminarmodel){
        $this->validate($request,[
            'nim' => 'required',
            'tgl_pembayaran' => 'required',
            'nominal' => 'required|numeric',
        ],[
            'nim.required' => 'Mahasiswa Harus Diisi',
            'tgl_pembayaran.required' => 'Tanggal Pembayran Harus Diisi',
            'nominal.required' => 'Nominal Harus Diisi',
            'nominal.numeric' => 'Nominal Harus Berupa Angka'
        ]);
        $simpan = $seminarmodel->create([
            'id_pembayaranseminar' => $request->id_pembayaranseminar,
            'nim' => $request->nim,
            'tgl_pembayaran' => $request->tgl_pembayaran,
            'nominal' => $request->nominal,
            'keterangan' => $request->keterangan,
            'penerima' => $request->penerima,
            'status' => 1
        ]);
        
        $saldo_akhir = SaldoModel::latest('nomor')->first();
        $saldo_akhir = $saldo_akhir->saldo;
        $total = $saldo_akhir + $request->nominal;
        
        $saldo = SaldoModel::create([
            'jumlah' => $request->nominal,
            'keterangan' => 'Seminar',
            'kategori' => 'Pemasukan',
            'saldo' => $total,
            'created_at' => now(),
        ]);
        
        $datacalonmhs = MahasiswaModel::where('nim', $request->nim)->update([
            'id_pembayaranseminar' => $request->id_pembayaranseminar,
        ]);

        if (!$simpan->exists) {
            toastr()->error('Pembayaran Seminar Gagal');
            return redirect()->route('seminar')->with('error', 'Data Gagal Disimpan');
        }
        toastr()->error('Pembayaran Seminar Gagal');
        return redirect()->route('seminar')->with('success', 'Data Berhasil Disimpan');
    }
}