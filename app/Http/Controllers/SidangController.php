<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MahasiswaModel;
use App\SidangModel;
use App\SaldoModel;
use App\User;

class SidangController extends Controller
{
    public function index(){
        $datamahasiswa = MahasiswaModel::all()->where('bayarsidang',2);
        $data = SidangModel::all();
        $total_pembayaransidang = SidangModel::all()->sum('nominal');
        $total_mhs = MahasiswaModel::where('bayarsidang',1)->count('id_sidang');
        $id_sidang = SidangModel::latest('id_sidang')->get()->first();
        $penerima = User::all();
        
        return view('admin.pemasukan.sidang.index', compact('datamahasiswa','data','total_pembayaransidang','total_mhs','id_sidang','penerima'));
    }

    public function store(Request $request, SidangModel $sidangmodel){
        $this->validate($request,[
            'nim' => 'required',
            'tgl_pembayaran' => 'required',
            'nominal' => 'required|numeric',
        ],[
            'nim.required' => 'Mahasiswa Harus Diisi',
            'tgl_pembayaran.required' => 'Tanggal Pembayran Harus Diisi',
            'nominal.required' => 'Nominal Harus Diisi',
            'nominal.numeric' => 'Nominal Harus Berupa Angka'
        ]);
        $simpan = $sidangmodel->create([
            'id_sidang' => $request->id_sidang,
            'nim' => $request->nim,
            'tgl_pembayaran' => $request->tgl_pembayaran,
            'nominal' => $request->nominal,
            'keterangan' => $request->keterangan,
            'penerima' => $request->penerima,
            'status' => 1
        ]);

        $saldo_akhir = SaldoModel::latest('nomor')->first();
        $saldo_akhir = $saldo_akhir->saldo;
        $total = $saldo_akhir + $request->nominal;
        
        $saldo = SaldoModel::create([
            'jumlah' => $request->nominal,
            'keterangan' => 'Sidang',
            'kategori' => 'Pemasukan',
            'saldo' => $total,
            'created_at' => now(),
        ]);

        $datacalonmhs = MahasiswaModel::where('nim', $request->nim)->update([
            'id_sidang' => $request->id_sidang,
            'bayarsidang'=> 1,
        ]);

        if (!$simpan->exists) {
            toastr()->error('Pembayaran Sidang Gagal');
            return redirect()->route('sidang')->with('error', 'Data Gagal Disimpan');
        }
        toastr()->success('Pembayaran Sidang Berhasil');
        return redirect()->route('sidang')->with('success', 'Data Berhasil Disimpan');
    }
}