<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MahasiswaModel;
use App\DaftarModel;
use App\SeminarModel;
use App\SidangModel;
use App\WisudaModel;
use App\SPPModel;
use App\SaldoModel;
use App\PengeluaranModel;
use Alert;

class DashboardController extends Controller
{
    public function index(){
        $mhs = MahasiswaModel::all()->count();
        $total_Pendaftaran = DaftarModel::all()->sum('nominal');
        $total_seminar = SeminarModel::all()->sum('nominal');
        $total_sidang = SidangModel::all()->sum('nominal');
        $total_wisuda = WisudaModel::all()->sum('nominal');
        $total_spp = SPPModel::all()->sum('nominal');
        $data = SaldoModel::all();

        $total_pemasukan = $total_Pendaftaran + $total_seminar + $total_sidang + $total_wisuda + $total_spp;
        $total_saldo = SaldoModel::latest('nomor')->first();
        $total = $total_saldo->saldo;
        $total_pengeluaran = PengeluaranModel::all()->sum('nominal');

        $jml1 = SaldoModel::where('kategori','Pemasukan')->get();
        $jml2 = SaldoModel::where('kategori','Pengeluaran')->get();
        
        return view('admin.index',compact('mhs', 'total_pemasukan','total','data','total_pengeluaran','jml1','jml2'));
    }

    public function masterpemasukan(){
        $pemasukan = SaldoModel::where('kategori','Pemasukan')->get();
        $jml = SaldoModel::where('kategori','Pemasukan')->sum('jumlah');
        $aktip='active';
        return view('admin.pemasukan.masterpemasukan',compact('pemasukan','jml','aktip'));
    }

    public function masterpengeluaran(){
        $pengeluaran = SaldoModel::where('kategori','Pengeluaran')->get();
        $jml = SaldoModel::where('kategori','Pengeluaran')->sum('jumlah');
        $aktip='active';
        return view('admin.pengeluaran.masterpengeluaran',compact('pengeluaran','jml','aktip'));
    }
}