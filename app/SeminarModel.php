<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeminarModel extends Model
{
    protected $table = 'seminar';
    protected $fillable = ['id_pembayaranseminar','tgl_pembayaran','nim','nominal','status','penerima','keterangan'];

    public function haveMahasiswa(){
        return $this->hasMany(MahasiswaModel::class, 'nim', 'nim');
    }
}