<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MahasiswaModel extends Model
{
    protected $table = 'mahasiswa';

    protected $fillable = ['nim','nm_mhs','alamat','id_kelas','id_jurusan','id_spp','no_telp','email','TA','status','id_pembayaran','id_pembayaranwisuda','id_pembayaranseminar','x_spp','bayarsidang','bayarwisuda','bayarseminarKP','bayarseminarUP'];

    public function haveKelas()
    {
        return $this->belongsTo(KelasModel::class, 'id_kelas', 'id_kelas');
    }
    
    public function haveJurusan()
    {
        return $this->belongsTo(JurusanModel::class, 'id_jurusan', 'id_jurusan');
    }

    public function hasManySPP()
    {
        return $this->hasMany(SppModel::class, 'id_spp', 'id_spp');
    }

}