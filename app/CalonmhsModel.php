<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\DaftarModel;

class CalonmhsModel extends Model
{
    protected $table = 'calon_mhs';
    protected $fillable = ['id_calonmhs','nama','tgl_lahir','id_jurusan','alamat','no_telp','email','TA'];

    public function hasManyDaftar(){
        return $this->hasMany(DaftarModel::class, 'id_calonmhs', 'id_calonmhs');
    }
}