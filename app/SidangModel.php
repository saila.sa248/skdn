<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SidangModel extends Model
{
    protected $table = 'sidang';

    protected $fillable = ['id_sidang','nim','nominal','tgl_pembayaran','status','penerima','keterangan'];

    public function haveMahasiswa()
    {
        return $this->hasMany(MahasiswaModel::class, 'nim', 'nim');
    }
}