<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PengeluaranModel extends Model
{
    protected $table = 'pengeluaran';
    protected $fillable = ['kode_pengeluaran','tgl_pengeluaran','kategori','nominal','status','penerima','keterangan','p_jawab'];
}