<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JurusanModel extends Model
{
    protected $table = 'jurusan';

    protected $fillable = ['id_jurusan','nm_jurusan','status'];

    public function hasManyMahasiswa()
    {
        return $this->hasMany(MahasiswaModel::class, 'id_jurusan', 'id_jurusan');
    }

}