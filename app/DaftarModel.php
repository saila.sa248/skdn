<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CalonmhsModel;

class DaftarModel extends Model
{
    protected $table = 'pendaftaran';
    protected $fillable = ['id_pembayaran','tgl_pembayaran','id_calonmhs','nominal','status','penerima','keterangan'];

    public function haveCalonmhs(){
        return $this->hasMany(CalonmhsModel::class, 'id_calonmhs', 'id_calonmhs');
    }
}