<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WisudaModel extends Model
{
    protected $table = 'wisuda';

    protected $fillable = ['id_pembayaranwisuda','nim','nominal','tgl_pembayaran','status','penerima','keterangan'];

    public function haveMahasiswa()
    {
        return $this->hasMany(MahasiswaModel::class, 'nim', 'nim');
    }
}
