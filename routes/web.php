<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

// Route::get('/', function () {
//     return view('welcome');
// });

// LOGIN
Auth::routes();
Route::get('/', 'AuthController@index')->name('login');
Route::post('/signin', 'AuthController@sendLoginRequest')->name('ceklogin');
Route::get('/dashboard', 'DashboardController@index' )->name('dashboard');

// MAHASISWA
Route::get('/mahasiswa', 'MahasiswaController@index')->name('mahasiswa');
Route::get('/mahasiswa/tambah', 'MahasiswaController@tambahmhs')->name('tambahmhs');
Route::post('/mahasiswa/post','MahasiswaController@postmhs')->name('postmhs');
Route::get('/mahasiswa/edit/{id}','MahasiswaController@editMhs')->name('editmhs');
Route::post('/mahasiswa/update/{id}','MahasiswaController@updateMhs')->name('updatemhs');
Route::post('/mahasiswa/delete/{id}','MahasiswaController@deleteMhs')->name('deletemhs');
Route::get('/detail-mahasiswa/{id}','MahasiswaController@show')->name('detail-mahasiswa');

// KELAS
Route::get('/kelas','KelasController@index')->name('kelas');
Route::post('/kelas/postkelas','KelasController@postkelas')->name('postkelas');
Route::get('/kelas/edit/{id}','KelasController@editKls')->name('editkls');
Route::post('/kelas/update/{id}','KelasController@updateKls')->name('updatekls');
Route::post('/kelas/delete/{id}','KelasController@deleteKls')->name('deletekls');

// JURUSAN
Route::get('/jurusan','JurusanController@index')->name('jurusan');
Route::post('/jurusan/postjurusan','JurusanController@postjurusan')->name('postjurusan');

// SPP
Route::get('/spp','SPPController@index')->name('spp');
Route::get('/spp/alldata','SPPController@alldata')->name('alldata');
Route::post('pembayaran-spp','SPPController@store')->name('pembayaran-spp');

// PEMBAYARAN PENDAFTARAN
Route::get('/bayardaftar','DaftarController@index')->name('daftar');
Route::post('pembayaran-pendaftaran','DaftarController@store')->name('pembayaran-pendaftaran');
Route::get('daftar-invoice/{id}','DaftarController@invoice')->name('daftar-invoice');

// PEMBAYARAN SIDANG
Route::get('bayarsidang','SidangController@index')->name('sidang');
Route::post('pembayaran-sidang','SidangController@store')->name('pembayaran-sidang');

// PEMBAYARAN SEMINAR
Route::get('bayarseminar','SeminarController@index')->name('seminar');
Route::post('pembayaran-seminar','SeminarController@store')->name('pembayaran-seminar');

// PEMBAYARAN WISUDA
Route::get('bayarwisuda','WisudaController@index')->name('wisuda');
Route::post('pembayaran-wisuda','WisudaController@store')->name('pembayaran-wisuda');

// PENGELUARAN
Route::get('pengeluaran','PengeluaranController@index')->name('pengeluaran');
Route::post('tambah_pengeluaran','PengeluaranController@store')->name('tambah_pengeluaran');

// MASTER PEMASUKAN
Route::get('pemasukan','DashboardController@masterpemasukan')->name('pemasukan');

// MASTER PENGELUARAN
Route::get('lap-pengeluaran','DashboardController@masterpengeluaran')->name('lap-pengeluaran');

// EX
Route::get('/home', 'HomeController@index')->name('home');

//email
Route::get('/email', function () {
    return view('send_email');
});
Route::post('/sendEmail', 'Email@sendEmail');