<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersSeeder::class);
        $this->call(PendaftaranSeeder::class);
        $this->call(KelasSeeder::class);
        $this->call(JurusanSeeder::class);
        $this->call(SppSeeder::class);
        $this->call(SidangSeeder::class);
        $this->call(SeminarSeeder::class);
        $this->call(WisudaSeeder::class);
        $this->call(MahasiswaSeeder::class);
        $this->call(CalonmhsSeeder::class);
        $this->call(PengeluaranSeeder::class);
        $this->call(SaldoSeeder::class);
    }
}