<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CalonmhsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('calon_mhs')->insert(array(
            array(
                'id_calonmhs' => 'CM001',
                'nama' => 'Anggi Moch Alpiansyah',
                'tgl_lahir' => '1996-01-01',
                'id_jurusan' => 'J001',
                'id_kelas' => 'K001',
                'alamat' => 'Ciparay',
                'no_telp' => '0812121212',
                'email' => 'anggi@gmail.com',
                'TA' => Carbon::parse('2017-01-01'),
                'status' => 1,
                'bayardaftar' => 1,
                'created_at'=> now()
            ),
            array(
                'id_calonmhs' => 'CM002',
                'nama' => 'Dean Ahman Jaelani',
                'tgl_lahir' => '1996-01-01',
                'id_jurusan' => 'J001',
                'id_kelas' => 'K001',
                'alamat' => 'Kiaracondong',
                'no_telp' => '0812121213',
                'email' => 'dean@gmail.com',
                'TA' => Carbon::parse('2017-01-01'),
                'status' => 1,
                'bayardaftar' => 2,
                'created_at'=> now()
            ),
            array(
                'id_calonmhs' => 'CM003',
                'nama' => 'Elliniar',
                'tgl_lahir' => '1996-01-01',
                'id_jurusan' => 'J001',
                'id_kelas' => 'K001',
                'alamat' => 'Rancaekek',
                'no_telp' => '0812121213',
                'email' => 'elliniar@gmail.com',
                'TA' => Carbon::parse('2017-01-01'),
                'status' => 1,
                'bayardaftar' => 2,
                'created_at'=> now()
            ),
            array(
                'id_calonmhs' => 'CM004',
                'nama' => 'Eman Sulaeman',
                'tgl_lahir' => '1996-01-01',
                'id_jurusan' => 'J001',
                'id_kelas' => 'K001',
                'alamat' => 'Ciamis',
                'no_telp' => '0812121213',
                'email' => 'Eman@gmail.com',
                'TA' => Carbon::parse('2017-01-01'),
                'status' => 1,
                'bayardaftar' => 2,
                'created_at'=> now()
            ),
            array(
                'id_calonmhs' => 'CM005',
                'nama' => 'Esya Abdul Manan',
                'tgl_lahir' => '1996-01-01',
                'id_jurusan' => 'J001',
                'id_kelas' => 'K001',
                'alamat' => 'Cibiru',
                'no_telp' => '0812121213',
                'email' => 'Esya@gmail.com',
                'TA' => Carbon::parse('2017-01-01'),
                'status' => 1,
                'bayardaftar' => 2,
                'created_at'=> now()
            ),
            array(
                'id_calonmhs' => 'CM006',
                'nama' => 'Fikri Maulana',
                'tgl_lahir' => '1996-01-01',
                'id_jurusan' => 'J001',
                'id_kelas' => 'K001',
                'alamat' => 'Ciwidey',
                'no_telp' => '0812121213',
                'email' => 'Fikri@gmail.com',
                'TA' => Carbon::parse('2017-01-01'),
                'status' => 1,
                'bayardaftar' => 2,
                'created_at'=> now()
            ),
            array(
                'id_calonmhs' => 'CM007',
                'nama' => 'Fitriana Nurlatifah',
                'tgl_lahir' => '1996-01-01',
                'id_jurusan' => 'J001',
                'id_kelas' => 'K001',
                'alamat' => 'Cikutra',
                'no_telp' => '0812121213',
                'email' => 'Fitriana@gmail.com',
                'TA' => Carbon::parse('2017-01-01'),
                'status' => 1,
                'bayardaftar' => 2,
                'created_at'=> now()
            ),
            array(
                'id_calonmhs' => 'CM008',
                'nama' => 'Gita Utari',
                'tgl_lahir' => '1996-01-01',
                'id_jurusan' => 'J001',
                'id_kelas' => 'K001',
                'alamat' => 'Garut',
                'no_telp' => '0812121213',
                'email' => 'Gita@gmail.com',
                'TA' => Carbon::parse('2017-01-01'),
                'status' => 1,
                'bayardaftar' => 2,
                'created_at'=> now()
            ),
            array(
                'id_calonmhs' => 'CM009',
                'nama' => 'M.Reza Airlangga',
                'tgl_lahir' => '1996-01-01',
                'id_jurusan' => 'J001',
                'id_kelas' => 'K001',
                'alamat' => 'Cimahi',
                'no_telp' => '0812121213',
                'email' => 'RezaA@gmail.com',
                'TA' => Carbon::parse('2017-01-01'),
                'status' => 1,
                'bayardaftar' => 2,
                'created_at'=> now()
            ),
            array(
                'id_calonmhs' => 'CM010',
                'nama' => 'Moh Fahmi Gunawan',
                'tgl_lahir' => '1996-01-01',
                'id_jurusan' => 'J001',
                'id_kelas' => 'K001',
                'alamat' => 'Majalaya',
                'no_telp' => '0812121213',
                'email' => 'Fahmi@gmail.com',
                'TA' => Carbon::parse('2017-01-01'),
                'status' => 1,
                'bayardaftar' => 2,
                'created_at'=> now()
            ),
            array(
                'id_calonmhs' => 'CM011',
                'nama' => 'Nurhadi Firmansyah',
                'tgl_lahir' => '1996-01-01',
                'id_jurusan' => 'J001',
                'id_kelas' => 'K001',
                'alamat' => 'Ciparay',
                'no_telp' => '0812121213',
                'email' => 'Hadi@gmail.com',
                'TA' => Carbon::parse('2017-01-01'),
                'status' => 1,
                'bayardaftar' => 2,
                'created_at'=> now()
            ),
            array(
                'id_calonmhs' => 'CM012',
                'nama' => 'Pian Sopian',
                'tgl_lahir' => '1996-01-01',
                'id_jurusan' => 'J001',
                'id_kelas' => 'K001',
                'alamat' => 'Ciparay',
                'no_telp' => '0812121213',
                'email' => 'Pian@gmail.com',
                'TA' => Carbon::parse('2017-01-01'),
                'status' => 1,
                'bayardaftar' => 2,
                'created_at'=> now()
            ),
            array(
                'id_calonmhs' => 'CM013',
                'nama' => 'Reza Gunawan',
                'tgl_lahir' => '1996-01-01',
                'id_jurusan' => 'J001',
                'id_kelas' => 'K001',
                'alamat' => 'Tasik',
                'no_telp' => '0812121213',
                'email' => 'RezaG@gmail.com',
                'TA' => Carbon::parse('2017-01-01'),
                'status' => 1,
                'bayardaftar' => 2,
                'created_at'=> now()
            ),
            array(
                'id_calonmhs' => 'CM014',
                'nama' => 'Rifal Agrian',
                'tgl_lahir' => '1996-01-01',
                'id_jurusan' => 'J001',
                'id_kelas' => 'K001',
                'alamat' => 'Ciparay',
                'no_telp' => '0812121213',
                'email' => 'Rifal@gmail.com',
                'TA' => Carbon::parse('2017-01-01'),
                'status' => 1,
                'bayardaftar' => 2,
                'created_at'=> now()
            ),
            array(
                'id_calonmhs' => 'CM015',
                'nama' => 'Rifki Zulfani',
                'tgl_lahir' => '1996-01-01',
                'id_jurusan' => 'J001',
                'id_kelas' => 'K001',
                'alamat' => 'Ciwidey',
                'no_telp' => '0812121213',
                'email' => 'Rifki@gmail.com',
                'TA' => Carbon::parse('2017-01-01'),
                'status' => 1,
                'bayardaftar' => 2,
                'created_at'=> now()
            ),
            array(
                'id_calonmhs' => 'CM016',
                'nama' => 'Saila Saadiah Amanatillah',
                'tgl_lahir' => '1996-01-01',
                'id_jurusan' => 'J001',
                'id_kelas' => 'K001',
                'alamat' => 'Cililin',
                'no_telp' => '0812121213',
                'email' => 'Saila@gmail.com',
                'TA' => Carbon::parse('2017-01-01'),
                'status' => 1,
                'bayardaftar' => 2,
                'created_at'=> now()
            ),
            array(
                'id_calonmhs' => 'CM017',
                'nama' => 'Sani Rianto',
                'tgl_lahir' => '1996-01-01',
                'id_jurusan' => 'J001',
                'id_kelas' => 'K001',
                'alamat' => 'Bojongsoang',
                'no_telp' => '0812121213',
                'email' => 'Sani@gmail.com',
                'TA' => Carbon::parse('2017-01-01'),
                'status' => 1,
                'bayardaftar' => 2,
                'created_at'=> now()
            ),
            array(
                'id_calonmhs' => 'CM018',
                'nama' => 'Setyo Dwi Cahyo',
                'tgl_lahir' => '1996-01-01',
                'id_jurusan' => 'J001',
                'id_kelas' => 'K001',
                'alamat' => 'Cimahi',
                'no_telp' => '0812121213',
                'email' => 'Setyo@gmail.com',
                'TA' => Carbon::parse('2017-01-01'),
                'status' => 1,
                'bayardaftar' => 2,
                'created_at'=> now()
            ),
            array(
                'id_calonmhs' => 'CM019',
                'nama' => 'Susi Nursyahadah',
                'tgl_lahir' => '1996-01-01',
                'id_jurusan' => 'J001',
                'id_kelas' => 'K001',
                'alamat' => 'Sumedang',
                'no_telp' => '0812121213',
                'email' => 'Susi@gmail.com',
                'TA' => Carbon::parse('2017-01-01'),
                'status' => 1,
                'bayardaftar' => 2,
                'created_at'=> now()
            ),
            array(
                'id_calonmhs' => 'CM020',
                'nama' => 'Zaref Suhendar',
                'tgl_lahir' => '1996-01-01',
                'id_jurusan' => 'J001',
                'id_kelas' => 'K001',
                'alamat' => 'Bandung',
                'no_telp' => '0812121213',
                'email' => 'Zaref@gmail.com',
                'TA' => Carbon::parse('2017-01-01'),
                'status' => 1,
                'bayardaftar' => 2,
                'created_at'=> now()
            ),
            ));
    }
}