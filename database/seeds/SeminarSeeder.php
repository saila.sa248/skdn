<?php

use Illuminate\Database\Seeder;

class SeminarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('seminar')->insert(array(
            array(
                'id_pembayaranseminar' => 'PSE001',
                'tgl_pembayaran' => now(),
                'nim' => '172101032',
                'nominal' => 100000,
                'keterangan' => 'Terverikasi',
                'penerima' => 'Saila SA',
                'status' =>1
            ),
        ));
    }
}