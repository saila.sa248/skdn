<?php

use Illuminate\Database\Seeder;

class PendaftaranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pendaftaran')->insert(array(
            array(
                'id_pembayaran' => 'PD001',
                'tgl_pembayaran' => now(),
                'id_calonmhs' => 'CM001',
                'nominal' => 100000,
                'keterangan' => 'Terverikasi',
                'penerima' => 'Saila SA',
                'status' =>1
            ),
        ));
    }
}