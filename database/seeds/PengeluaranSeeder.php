<?php

use Illuminate\Database\Seeder;

class PengeluaranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pengeluaran')->insert(array(
            array(
                'kode_pengeluaran' => 'PG001',
                'tgl_pengeluaran' => now(),
                'Kategori' => 'ATK',
                'nominal' => 0,
                'p_jawab' => 'Setyo',
                'penerima' => 'Saila SA',
                'keterangan' => 'Pembelian Spidol 2 Pack',
                'status' =>1
            ),
        ));
    }
}