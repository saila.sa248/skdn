<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class SppSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('spp')->insert(array(
            array(
                'id_spp' =>'SPP001',
                'nim' => '172101032',
                'nominal' =>100000,
                'tgl_pembayaran' => Carbon::parse('2017-01-01'),
                'bulan_pembayaran' => 'Januari',
                'penerima' => 'Saila SA',
                'keterangan' => 'Lunas',
                'status' => 1
            ),
        ));
    }
}