<?php

use Illuminate\Database\Seeder;

class SaldoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * 
     * @return void
     */
    public function run()
    {
        DB::table('saldo')->insert(array(
            array(
                'nomor' => 1,
                'jumlah' => 100000,
                'saldo' => 100000,
                'kategori' => 'Pemasukan',
                'keterangan' => 'SPP',
                'created_at' => now(),
                'updated_at' => now()
                ),
            array(
                'nomor' => 2,
                'jumlah' => 100000,
                'saldo' => 200000,
                'kategori' => 'Pemasukan',
                'keterangan' => 'Pendaftaran',
                'created_at' => now(),
                'updated_at' => now()
                ),
            array(
                'nomor' => 3,
                'jumlah' => 100000,
                'saldo' => 300000,
                'kategori' => 'Pemasukan',
                'keterangan' => 'Seminar',
                'created_at' => now(),
                'updated_at' => now()
                ),
            array(
                'nomor' => 4,
                'jumlah' => 100000,
                'saldo' => 400000,
                'kategori' => 'Pemasukan',
                'keterangan' => 'Sidang',
                'created_at' => now(),
                'updated_at' => now()
                ),
            array(
                'nomor' => 5,
                'jumlah' => 100000,
                'saldo' => 500000,
                'kategori' => 'Pemasukan',
                'keterangan' => 'Wisuda',
                'created_at' => now(),
                'updated_at' => now()
                ),
        ));
    }
}