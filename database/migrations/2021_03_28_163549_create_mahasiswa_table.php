<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMahasiswaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mahasiswa', function (Blueprint $table) {
            $table->string('nim')->primary();
            $table->string('id_kelas');
            $table->foreign('id_kelas')->references('id_kelas')->on('kelas');
            $table->string('id_jurusan');
            $table->foreign('id_jurusan')->references('id_jurusan')->on('jurusan');
            $table->string('id_spp')->nullable()->default(0);
            $table->foreign('id_spp')->references('id_spp')->on('spp');
            $table->string('id_sidang')->nullable()->default(0);
            $table->foreign('id_sidang')->references('id_sidang')->on('sidang');
            $table->string('id_pembayaranwisuda')->nullable()->default(0);
            $table->foreign('id_pembayaranwisuda')->references('id_pembayaranwisuda')->on('wisuda');
            $table->string('id_pembayaranseminar')->nullable()->default(0);
            $table->foreign('id_pembayaranseminar')->references('id_pembayaranseminar')->on('seminar');
            $table->string('nm_mhs',30);
            $table->string('alamat',100);
            $table->string('no_telp',15);
            $table->string('email',30);
            $table->date('TA');
            $table->tinyinteger('x_spp');
            $table->tinyinteger('status');
            $table->tinyinteger('bayarsidang');
            $table->tinyinteger('bayarwisuda');
            $table->tinyinteger('bayarseminarKP');
            $table->tinyinteger('bayarseminarUP');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mahasiswa');
    }
}