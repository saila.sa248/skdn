<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCalonMhsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calon_mhs', function (Blueprint $table) {
            $table->string('id_calonmhs')->primary();
            $table->string('nama');
            $table->date('tgl_lahir');
            $table->string('id_jurusan');
            $table->string('id_kelas');
            $table->string('alamat',100);
            $table->string('no_telp',13);
            $table->string('email');
            $table->date('TA');
            $table->tinyinteger('status');
            $table->tinyinteger('bayardaftar');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calon_mhs');
    }
}