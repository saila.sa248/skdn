<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWisudaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wisuda', function (Blueprint $table) {
            $table->string('id_pembayaranwisuda')->primary();
            $table->date('tgl_pembayaran');
            $table->string('nim');
            $table->integer('nominal');
            $table->string('keterangan');
            $table->string('penerima');
            $table->tinyinteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wisuda');
    }
}