<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="{{asset('/style/img/DNBS.ico')}}">
    <title>Sistem Keuangan - STMIK Dharma Negara</title>

    <!-- NEW CSS -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
    <link rel=" preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{asset ('style/css/login1.css') }}">
</head>
<script>
(function titleScroller(text) {
    document.title = text;
    setTimeout(function() {
        titleScroller(text.substr(1) + text.substr(0, 1));
    }, 500);
}(" Login - Sistem Keuangan Dharma Negara |"));
</script>

<body>
    @include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])
    @include('sweetalert::alert')
    <div class="container-fluid p-0 ">
        <div class="row d-flex justify-content-center">
            <div class="col-lg-3 col-md-6 col-xs-6 p-5">
                <form class="form-signin" action="{{route('ceklogin')}}" method="POST">
                    <img src="{{asset('/style/img/Logonew.png')}}" alt="logo" width="100%" class="img-fluid mb-5">
                    @csrf
                    <div class="form-group">
                        <input type="email" id="email" name="email" class="form-control" placeholder="Email address"
                            required autofocus>
                    </div>
                    <div class="form-group">
                        <input type="password" id="password" name="password" class="form-control" placeholder="Password"
                            required>
                    </div>
                    <button class="btn btn-sm btn-blues btn-block text-uppercase" type="submit">Masuk</button>
                </form>
            </div>
        </div>
    </div>
    <footer class="footer py-3 bg-dark text-muted fixed-bottom">
        <div class="container">
            <div class="col-md-12 text-center">
                <span class="text-sm">&copy 2020 - <?php echo date("Y"); ?> Saila S Amanatillah - All
                    Rights Reserved. | <strong>Sistem Keuangan - STMIK Dharma Negara </strong></span>
            </div>
        </div>
    </footer>
    <!-- <div class="col-lg-12 fixed-bottom p-0">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 300">
            <path fill="#0f4275" fill-opacity="1"
                d="M0,96L80,133.3C160,171,320,245,480,229.3C640,213,800,107,960,74.7C1120,43,1280,85,1360,106.7L1440,128L1440,320L1360,320C1280,320,1120,320,960,320C800,320,640,320,480,320C320,320,160,320,80,320L0,320Z">
            </path>
        </svg>
    </div> -->
</body>

<script src="{{asset ('style/js/jquery-3.4.1.js')}}"></script>
<script src="{{asset ('style/js/bootstrap.min.js')}}"></script>

</html>