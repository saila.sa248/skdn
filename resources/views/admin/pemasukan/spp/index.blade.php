@extends('../admin.layouts.master')
@section('content')
@include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])
@include('sweetalert::alert')
<!-- Bagian modal brosur -->
<div id="mydaftar" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p class="modal-title">Invoice</p>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body p-0 m-0">
                <form action="{{ url('/sendEmail') }}" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" id="email" name="email">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" class="form-control" id="nama" name="nama">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Judul</label>
                                    <input type="text" class="form-control" id="judul" name="judul" value="Invoice"
                                        readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Pembayaran</label>
                                    <input type="text" class="form-control" id="pesan" name="pesan" value="SPP"
                                        readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer text-dark float-right">
                        <button type="submit" class="btn btn-warning float-right btn-sm" href="#">Kirim</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row mt-3">
        <div class="col-lg-3 col-md-3 ">
            <div class="small-box bg-blues p-2">
                <div class="inner">
                    <h3>@currency($total_spp)</h3>
                    <p>Total Pemasukan SPP</p>
                </div>
            </div>
            <div class="small-box bg-warning p-2">
                <div class="inner">
                    <h3>{{$total_mhs}} Orang</h3>
                    <p>Total Mahasiswa </br> yang sudah membayar</p>
                </div>
                <div class="icon">
                    <i class="ion ion-ios-people"></i>
                </div>
                <!-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
            </div>
        </div>

        <section class="col-lg-9 col-md-9 connectedSortable">
            <div class="card">
                <div class="card-header bg-blues">
                    <h3 class="card-title">Pembayaran SPP</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <form action="{{route('pembayaran-spp')}}" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    @php
                                    $dd = substr($idspp->id_spp,3);
                                    $hasil = $dd + 1;
                                    $hasil1 = str_pad($hasil, 3, "00", STR_PAD_LEFT)
                                    @endphp
                                    <label for="id_spp">Kode Pembayaran SPP</label>
                                    <input type="text" class="form-control" id="id_spp" name="id_spp"
                                        placeholder="SPP{{$hasil1}}" value="SPP{{$hasil1}}" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>NIM</label>
                                <select class="form-control select2 select2-ok" name="nim">
                                    @foreach($data as $xa)
                                    <option value="{{$xa->nim}}">
                                        {{$xa->nim}} - {{$xa->nm_mhs}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Penerima Pembayaran</label>
                                    <input type="text" class="form-control" id="penerima" name="penerima"
                                        value="{{$penerima[0]->name}}" placeholder="{{$penerima[0]->name}}" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Bulan Pembayaran</label>
                                    <select id="bulan_pembayaran" name="bulan_pembayaran" class="form-control select2"
                                        style="width: 100%;">
                                        <option value="Januari" selected="selected">Januari</option>
                                        <option value="Februari">Februari</option>
                                        <option value="Maret">Maret</option>
                                        <option value="April">April</option>
                                        <option value="Mei">Mei</option>
                                        <option value="Juni">Juni</option>
                                        <option value="Juli">Juli</option>
                                        <option value="Agustus">Agustus</option>
                                        <option value="September">September</option>
                                        <option value="Oktober">Oktober</option>
                                        <option value="November">November</option>
                                        <option value="Desember">Desember</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Tanggal Pembayaran</label>
                                    <input type=date class="form-control" id="tgl_pembayaran" name="tgl_pembayaran"
                                        value="{{date('d M Y', strtotime(now()))}}">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="id_spp">Nominal</label>
                                    <div class="input-group">
                                        <div class="input-group-append">
                                            <span class="input-group-text">Rp. </span>
                                        </div>
                                        <input type="text" class="form-control" id="nominal" name="nominal"
                                            placeholder="Nominal">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="id_spp">Keterangan</label>
                                    <input type="text" class="form-control" id="keterangan" name="keterangan"
                                        placeholder="Keterangan">
                                </div>
                                <input type="hidden" name="x_spp" value="{{$data[0]->x_spp}}">
                                <button type="submit" class="btn btn-blues float-right">Bayar</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>

    <div class="card">
        <div class="card-header bg-blues">
            <h3 class="card-title">Logs Pembayaran SPP</h3>
        </div>
        <div class="card-body">
            <table id="example1" class="table table-bordered table-hover">
                <thead class="thead-light">
                    <tr>
                        <th>Kode SPP</th>
                        <th>NIM</th>
                        <th>Nama Mahasiswa</th>
                        <th>Bulan Pembayaran</th>
                        <th>Tanggal Pembayaran</th>
                        <th>Penerima</th>
                        <th>Nominal</th>
                        <th>Keterangan</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($dataSPP as $spp)
                    <tr>
                        <td>{{$spp->id_spp}}</td>
                        <td>{{$spp->nim}}</td>
                        <td>{{$spp->haveMahasiswa->nm_mhs}}</td>
                        <td>{{$spp->bulan_pembayaran}}</td>
                        <td>{{date('d M Y', strtotime($spp->tgl_pembayaran))}}</td>
                        <td>{{$spp->penerima}}</td>
                        <td>@currency($spp->nominal)</td>
                        <td>{{$spp->keterangan}}</td>
                        <td>
                            <a href="{{route('daftar-invoice',$spp->id_spp)}}" type="submit"
                                class="btn btn-blues float-right" data-toggle="modal"
                                data-target="#mydaftar">Invoice</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>Kode SPP</th>
                        <th>NIM</th>
                        <th>Nama Mahasiswa</th>
                        <th>Bulan Pembayaran</th>
                        <th>Tanggal Pembayaran</th>
                        <th>Penerima</th>
                        <th>Nominal</th>
                        <th>Keterangan</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    </section>
    <!-- </div> -->
</div>
@endsection