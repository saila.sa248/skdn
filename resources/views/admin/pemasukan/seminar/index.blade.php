@extends('admin.layouts.master')
@section('content')
@include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])
@include('sweetalert::alert')
<!-- Bagian modal brosur -->
<div id="mydaftar" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p class="modal-title">Invoice</p>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body p-0 m-0">
                <form action="{{ url('/sendEmail') }}" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" id="email" name="email">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" class="form-control" id="nama" name="nama">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Judul</label>
                                    <input type="text" class="form-control" id="judul" name="judul" value="Invoice"
                                        readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Pembayaran</label>
                                    <input type="text" class="form-control" id="pesan" name="pesan" value="Seminar">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer text-dark float-right">
                        <button type="submit" class="btn btn-warning float-right btn-sm" href="#">Kirim</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row mt-3">
        <div class="col-lg-3 col-md-3 ">
            <div class="small-box bg-blues p-2">
                <div class="inner">
                    <h3>@currency($total_pembayaranseminar)</h3>
                    <p>Total Pemasukan Pembayaran Seminar</p>
                </div>
            </div>
            <div class="small-box bg-warning p-2">
                <div class="inner">
                    <h3>{{$total_mhs}} orang</h3>
                    <p>Total Mahasiswa </br>yang sudah membayar</p>
                </div>
                <div class="icon">
                    <i class="ion ion-ios-people"></i>
                </div>
                <!-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
            </div>
        </div>

        <section class="col-lg-9 col-md-9 connectedSortable">
            <div class="card">
                <div class="card-header bg-blues">
                    <h3 class="card-title">Pembayaran Seminar</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <form action="{{route('pembayaran-seminar')}}" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    @php
                                    $dd = substr($id_pembayaranseminar->id_pembayaranseminar,3);
                                    $hasil = $dd + 1;
                                    $hasil1 = str_pad($hasil, 3, "00", STR_PAD_LEFT)
                                    @endphp
                                    <label for="id_pembayaran">Kode Pembayaran Seminar</label>
                                    <input type="text" class="form-control" id="id_pembayaranseminar"
                                        name="id_pembayaranseminar" value="PSE{{$hasil1}}" placeholder="PSE{{$hasil1}}"
                                        readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>ID dan Nama Calon Mahasiswa</label>
                                <select class="form-control select2 select2-ok" name="nim">
                                    @foreach($datamahasiswa as $xa)
                                    <option value="{{$xa->nim}}">
                                        {{$xa->nim}} - {{$xa->nm_mhs}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('nim'))
                                <span class="text-danger">{{ $errors->first('nim') }}</span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Tanggal Pembayaran</label>
                                    <input type="date" class="form-control" id="tgl_pembayaran" name="tgl_pembayaran"
                                        placeholder="Tanggal Pembayaran">
                                </div>
                                @if ($errors->has('tgl_pembayaran'))
                                <span class="text-danger">{{ $errors->first('tgl_pembayaran') }}</span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nominal</label>
                                    <div class="input-group">
                                        <div class="input-group-append">
                                            <span class="input-group-text">Rp. </span>
                                        </div>
                                        <input type="text" class="form-control" id="nominal" name="nominal"
                                            placeholder="Nominal">
                                    </div>
                                    @if ($errors->has('nominal'))
                                    <span class="text-danger">{{ $errors->first('nominal') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="id_spp">Penerima</label>
                                    <input type="text" class="form-control" id="penerima" name="penerima"
                                        value="{{$penerima[0]->name}}" placeholder="{{$penerima[0]->name}}" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="id_spp">Keterangan</label>
                                    <input type="text" class="form-control" id="keterangan" name="keterangan"
                                        placeholder="Keterangan">
                                </div>
                                <button type="submit" class="btn btn-blues float-right">Bayar</button>
                            </div>
                        </div>
                    </div>
            </div>
            </form>
        </section>
    </div>

    <div class="card">
        <div class="card-header bg-blues">
            <h3 class="card-title">Logs Pembayaran Seminar</h3>
        </div>
        <div class="card-body">
            <table id="example1" class="table table-bordered table-hover">
                <thead class="thead-light">
                    <tr>
                        <th>Kode Pembayaran</th>
                        <th>Tgl Pembayaran</th>
                        <th>NIM</th>
                        <th>Nama</th>
                        <th>Nominal</th>
                        <th>Penerima</th>
                        <th>Keterangan</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $row)
                    <tr>
                        <td>{{$row->id_pembayaranseminar}}</td>
                        <td>{{$row->tgl_pembayaran}}</td>
                        <td>{{$row->nim}}</td>
                        <td>{{$row->haveMahasiswa[0]->nm_mhs}}</td>
                        <td>{{$row->nominal}}</td>
                        <td>{{$row->penerima}}</td>
                        <td>{{$row->keterangan}}</td>
                        @if($row->status == 1)
                        <td>Close</td>
                        @endif
                        <td>
                            <a href="{{route('daftar-invoice',$row->id_pembayaranseminar)}}" type="submit"
                                class="btn btn-blues float-right" data-toggle="modal"
                                data-target="#mydaftar">Invoice</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>Kode Pembayaran</th>
                        <th>Tgl Pembayaran</th>
                        <th>ID Mahasiswa Baru</th>
                        <th>Nama</th>
                        <th>Nominal</th>
                        <th>Penerima</th>
                        <th>Keterangan</th>
                        <th>Status</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    </section>
</div>
@endsection
<!-- onchange='isi_otomatis()' -->