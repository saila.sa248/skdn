@extends('../admin.layouts.master')
@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">

            <div class="col-sm-6">
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<div class="container-fluid">
    @toastr_css
    <!-- Main row -->
    <div class="row">
        <!-- right col -->
        <section class="col-lg-12 col-md-12 col-sm-12 tabel-font">
            <div class="card">
                <div class="card-header bg-blues">
                    <h3 class="card-title pt-2">Tabel Pemasukan</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-hover table-font">
                        <thead class=" thead-grey">
                            <tr>
                                <th class="text-center">NO</th>
                                <th class="text-center">Tanggal</th>
                                <th>Keterangan</th>
                                <th class="text-center">Jumlah</th>
                            </tr>
                        </thead>
                        <tbody style='font-size:6pt'>
                            @php
                            $i = 1;
                            @endphp
                            @foreach($pemasukan as $pms)
                            <tr>
                                <td class="text-center">{{$i++}}</td>
                                <td class="text-center">{{date('d M Y', strtotime($pms->created_at))}}</td>
                                <td>{{$pms->keterangan}}</td>
                                <td class="text-center">@currency($pms->jumlah)</td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan=3 class="text-center font-weight-bold">TOTAL</td>
                                <td class="bg-warning text-center font-weight-bold">@currency($jml)</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
        </section>


    </div>
    <!-- /.row (main row) -->
</div>
@jquery
@toastr_js
@toastr_render
@endsection