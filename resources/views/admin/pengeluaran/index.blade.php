@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
    <div class="row mt-3">
        <div class="col-lg-3 col-md-3 ">
            <div class="small-box bg-blues p-2">
                <div class="inner">
                    <h3>@currency($total_saldo)</h3>
                    <p>Total Saldo</p>
                </div>
            </div>
            <div class="small-box bg-warning p-2">
                <div class="inner">
                    <h3>@currency($tot_pengeluaran)</h3>
                    <p>Total Pengeluaran</p>
                </div>
            </div>
        </div>

        <section class="col-lg-9 col-md-9 connectedSortable">
            <div class="card">
                <div class="card-header bg-blues">
                    <h3 class="card-title">Pengeluaran</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <form action="{{route('tambah_pengeluaran')}}" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    @php
                                    $dd = substr($idpengeluaran->kode_pengeluaran,2);
                                    $hasil = $dd + 1;
                                    $hasil1 = str_pad($hasil, 3, "00", STR_PAD_LEFT)
                                    @endphp
                                    <label for="kode_pengeluaran">Kode Pengeluaran</label>
                                    <input type="text" class="form-control" id="kode_pengeluaran"
                                        name="kode_pengeluaran" value="PG{{$hasil1}}" placeholder="" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="kategori">Kategori Pengeluaran</label>
                                <select class="form-control select2 select2-ok" name="kategori" id="kategori">
                                    <option value="ATK">ATK</option>
                                    <option value="Konsumsi">Konsumsi</option>
                                    <option value="Komunikasi">Komunikasi</option>
                                    <option value="Transportasi">Transportasi</option>
                                    <option value="Dan Lain-lain">Dan Lain-lain</option>
                                </select>
                                @if ($errors->has('kategori'))
                                <span class="text-danger">{{ $errors->first('kategori') }}</span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tgl_pengeluaran">Tanggal Pengeluaran</label>
                                    <input type="date" class="form-control" id="tgl_pengeluaran" name="tgl_pengeluaran"
                                        value="{{date('d M Y', strtotime(now()))}}" onClick="getDate()">
                                    @if ($errors->has('tgl_pengeluaran'))
                                    <span class="text-danger">{{ $errors->first('tgl_pengeluaran') }}</span>
                                    @endif
                                </div>
                                <script>
                                function getDate() {
                                    var today = new Date();
                                    document.getElementById("tgl_pengeluaran").value = today.getFullYear() + '-' + (
                                        '0' +
                                        (
                                            today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-
                                        2);
                                }
                                </script>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nominal">Nominal Pengeluaran</label>
                                    <div class="input-group">
                                        <div class="input-group-append">
                                            <span class="input-group-text">Rp. </span>
                                        </div>
                                        <input type="text" class="form-control" id="nominal" name="nominal"
                                            placeholder="Nominal Pengeluaran">
                                    </div>
                                    @if ($errors->has('nominal'))
                                    <span class="text-danger">{{ $errors->first('nominal') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="p_jawab">Penanggung Jawab</label>
                                    <input type="text" class="form-control" id="p_jawab" name="p_jawab"
                                        placeholder="{{$penerima[0]->name}}" value="{{$penerima[0]->name}}   " readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="penerima">Penerima</label>
                                    <input type="text" class="form-control" id="penerima" name="penerima"
                                        placeholder="Yang Menerima Uang">
                                    @if ($errors->has('penerima'))
                                    <span class="text-danger">{{ $errors->first('penerima') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="keterangan">Keterangan</label>
                                    <textarea type="text" class="form-control" id="keterangan" name="keterangan"
                                        placeholder="Keterangan"></textarea>
                                    @if ($errors->has('keterangan'))
                                    <span class="text-danger">{{ $errors->first('keterangan') }}</span>
                                    @endif
                                </div>
                                <button type="submit" class="btn btn-blues float-right">Bayar</button>
                            </div>
                        </div>
                    </div>
            </div>
            </form>
        </section>
    </div>

    <div class="row pt-0">
        <section class="col-lg-12">
            <!-- <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Logs Pembayaran Seminar</h1>
                        </div>
                    </div>
                </div>
            </section> -->
            <div class="card">
                <div class="card-header bg-blues">
                    <h3 class="card-title">Logs Pengeluaran</h3>
                </div>
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Kode Pengeluaran</th>
                                <th>Tgl Pengeluaran</th>
                                <th>Kategori</th>
                                <th>Nominal</th>
                                <th>Penanggung Jawab</th>
                                <th>Penerima</th>
                                <th>Keterangan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($pengeluaran as $p)
                            <tr>
                                <td>{{$p->kode_pengeluaran}}</td>
                                <td>{{$p->tgl_pengeluaran}}</td>
                                <td>{{$p->kategori}}</td>
                                <td>@currency($p->nominal)</td>
                                <td>{{$p->p_jawab}}</td>
                                <td>{{$p->penerima}}</td>
                                <td>{{$p->keterangan}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>
@endsection
<!-- onchange='isi_otomatis()' -->