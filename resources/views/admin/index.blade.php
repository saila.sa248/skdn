@extends('admin.layouts.master')
@section('content')
@include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])
@include('sweetalert::alert')

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Sistem Keuangan Dharma Negara</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Beranda</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<div class="container-fluid">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-blues">
                <div class="inner">
                    <h3 style='font-size:15pt'>@currency($total_pemasukan)</h3>

                    <p>Pemasukan</p>
                </div>
                <div class="icon">
                    <i class="ion ion-plus-round"></i>
                </div>
                <a href="{{route('pemasukan')}}" class="small-box-footer">More info <i
                        class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-reds">
                <div class="inner">
                    <h3 style='font-size:15pt'>@currency($total_pengeluaran)</h3>
                    <p>Pengeluaran</p>
                </div>
                <div class="icon">
                    <i class="fas fa-share"></i>
                </div>
                <a href="{{route('pengeluaran')}}" class="small-box-footer">More info <i
                        class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-greeny">
                <div class="inner">
                    <h3 style='font-size:15pt'>@currency($total)</h3>
                    <p>Saldo</p>
                </div>
                <div class="icon">
                    <i class="ion ion-cash"></i>
                </div>
                <a href="#" class="small-box-footer"> <i class="fas fa-money-bill-alt"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
                <div class="inner">
                    <h3 style='font-size:15pt'>{{$mhs}}</h3>
                    <p>Total Mahasiswa</p>
                </div>
                <div class="icon">
                    <i class="ion ion-ios-people"></i>
                </div>
                <a href="{{route('mahasiswa')}}" class="small-box-footer">More info <i
                        class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
    </div>
    <!-- /.row -->
    <!-- Main row -->
    <div class="row">
        <section class="col-lg-12">
            <div class="card">
                <div class="card-header bg-blues">
                    <h3 class="card-title">Data Keuangan</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-hover">
                        <thead class="thead-light">
                            <tr class="col">
                                <th style="width:5%">Nomor</th>
                                <th style="width:20%">Tanggal</th>
                                <th style="width:20%">Keterangan</th>
                                <th style="width:10%">Debit</th>
                                <th style="width:10%">Kredit</th>
                                <th style="width:20%">Saldo</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $saldo)
                            <tr>
                                <td>{{$saldo->nomor}}</td>
                                <td>{{$saldo->created_at}}</td>
                                <td>{{$saldo->keterangan}}</td>
                                @if($saldo->kategori == 'Pemasukan')
                                <td>@currency($saldo->jumlah)</td>
                                @else
                                <td>-</td>
                                @endif
                                @if($saldo->kategori == 'Pengeluaran')
                                <td>@currency($saldo->jumlah)</td>
                                @else
                                <td>-</td>
                                @endif
                                <td>@currency($saldo->saldo)</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
        </section>
    </div>
    <!-- /.row (main row) -->
</div>

@endsection