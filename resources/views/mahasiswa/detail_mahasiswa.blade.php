@extends('admin.layouts.master')
@section('content')
<div class="container-fluid">
    @toastr_css
    <br>
    <section class="col-lg-12 col-md-12 connectedSortable">
        <div class="card">
            <div class="card-header bg-blues">
                <h3 class="card-title">Detail Mahasiswa</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>
            <table id="example1" class="table table-borderless table-font m-3">
                <tr>
                    <td width="1">NIM</td>
                    <td width="1">:</td>
                    <td>{{$data[0]->nim}}</td>
                </tr>
                <tr>
                    <td width="1">Nama</td>
                    <td width="1">:</td>
                    <td>{{$data[0]->nm_mhs}}</td>
                </tr>
                <tr>
                    <td width="1">Alamat</td>
                    <td width="1">:</td>
                    <td>{{$data[0]->alamat}}</td>
                </tr>
                <tr>
                    <td width="1">No HP</td>
                    <td width="1">:</td>
                    <td>{{$data[0]->no_telp}}</td>
                </tr>
                <tr>
                    <td width="1">Email</td>
                    <td width="1">:</td>
                    <td>{{$data[0]->email}}</td>
                </tr>
                <tr>
                    <td width="1">Kelas</td>
                    <td width="1">:</td>
                    <td>{{$data[0]->haveKelas->nm_kelas}}</td>
                </tr>
                <tr>
                    <td width="1">Jurusan</td>
                    <td width="1">:</td>
                    <td>{{$data[0]->haveJurusan->nm_jurusan}}</td>
                </tr>
                <tr>
                    <td width="1">Angkatan</td>
                    <td width="1">:</td>
                    <td>{{$data[0]->TA}}</td>
                </tr>
            </table>
        </div>
        <div class="card">
            <div class="card-header bg-warning">
                <h3 class="card-title font-weight-bold">Logs Pembayaran</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>
            <table id="example1" class="table table-bordered table-hover mt-2 table-font">
                <thead class=" thead-grey">
                    <tr>
                        <th class="bg-blues">SPP</th>
                    </tr>
                    <tr>
                        <th class="text-center">Sisa SPP</th>
                        <th>Kode SPP</th>
                        <th>Tanggal Pembayaran</th>
                        <th class="text-center">Bulan</th>
                        <th>Keterangan</th>
                        <th>Nominal</th>
                    </tr>
                </thead>
                <tbody style='font-size:6pt'>
                    <tr></tr>
                    @foreach($spp as $s)
                    <tr>
                        <td class="text-center">{{$s->haveMahasiswa->x_spp}}</td>
                        <td>{{$s->id_spp}}</td>
                        <td>{{date('d M Y', strtotime($s->bulan_pembayaran))}}</td>
                        <td class="text-center">{{date('M', strtotime($s->bulan_pembayaran))}}</td>
                        <td>{{$s->keterangan}}</td>
                        <td>@currency($s->nominal)</td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan=5 class="text-danger font-weight-bold text-right">Total</td>
                        <td>@currency($tot)</td>
                    </tr>
                </tfoot>
            </table>
            <table id="example1" class="table table-bordered table-hover mt-2 table-font">
                <thead class=" thead-grey">
                    <tr>
                        <th class="bg-blues">SEMINAR</th>
                    </tr>
                    <tr>
                        <th class="text-center">Kode Pembayaran</th>
                        <th>Tanggal Pembayaran</th>
                        <th class="text-center">Penerima</th>
                        <th>Keterangan</th>
                        <th>Nominal</th>
                    </tr>
                </thead>
                <tbody style='font-size:6pt'>
                    <tr></tr>
                    @foreach($sm as $sm)
                    <tr>
                        <td class="text-center">{{$sm->id_pembayaranseminar}}</td>
                        <td>{{date('d M Y', strtotime($sm->tgl_pembayaran))}}</td>
                        <td class="text-center">{{$sm->penerima}}</td>
                        <td>{{$sm->keterangan}}</td>
                        <td>@currency($sm->nominal)</td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan=4 class="text-danger font-weight-bold text-right">Total</td>
                        <td>@currency($totsm)</td>
                    </tr>
                </tfoot>
            </table>
            <table id="example1" class="table table-bordered table-hover mt-2 table-font">
                <thead class=" thead-grey">
                    <tr>
                        <th class="bg-blues">SIDANG</th>
                    </tr>
                    <tr>
                        <th class="text-center">Kode Pembayaran</th>
                        <th>Tanggal Pembayaran</th>
                        <th class="text-center">Penerima</th>
                        <th>Keterangan</th>
                        <th>Nominal</th>
                    </tr>
                </thead>
                <tbody style='font-size:6pt'>
                    <tr></tr>
                    @foreach($sd as $sd)
                    <tr>
                        <td class="text-center">{{$sd->id_sidang}}</td>
                        <td>{{date('d M Y', strtotime($sd->tgl_pembayaran))}}</td>
                        <td class="text-center">{{$sd->penerima}}</td>
                        <td>{{$sd->keterangan}}</td>
                        <td>@currency($sd->nominal)</td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan=4 class="text-danger font-weight-bold text-right">Total</td>
                        <td>@currency($totsd)</td>
                    </tr>
                </tfoot>
            </table>
            <table id="example1" class="table table-bordered table-hover mt-2 table-font">
                <thead class=" thead-grey">
                    <tr>
                        <th class="bg-blues">WISUDA</th>
                    </tr>
                    <tr>
                        <th class="text-center">Kode Pembayaran</th>
                        <th>Tanggal Pembayaran</th>
                        <th class="text-center">Penerima</th>
                        <th>Keterangan</th>
                        <th>Nominal</th>
                    </tr>
                </thead>
                <tbody style='font-size:6pt'>
                    <tr></tr>
                    @foreach($wd as $wd)
                    <tr>
                        <td class="text-center">{{$wd->id_pembayaranwisuda}}</td>
                        <td>{{date('d M Y', strtotime($wd->tgl_pembayaran))}}</td>
                        <td class="text-center">{{$wd->penerima}}</td>
                        <td>{{$wd->keterangan}}</td>
                        <td>@currency($wd->nominal)</td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan=4 class="text-danger font-weight-bold text-right">Total</td>
                        <td>@currency($totwd)</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </section>
</div>
@jquery
@toastr_js
@toastr_render
@endsection